require essioc

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "1000000")
epicsEnvSet("ENGINEER", "Rafael Baron <rafael.baron@ess.eu>")
epicsEnvSet("LOG_SERVER_NAME", "172.16.107.59")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# General prefix
epicsEnvSet("P", "PBI-BPM::")

############################################################################
# BPM01-IOC01 - MEBT-010 7 BPMs + DTL-010 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G1", "BPM01")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G1):Ctrl-$(EVR_DEV):")
epicsEnvSet("S1", "MEBT")             # Section1: MEBT
epicsEnvSet("S2", "DTL")              # Section2: DTL

# Single BPMs Crate 01
# MEBT-010 BPMs: 01 02 03 xx 05 06 07 08
# bpmmanager_single_bpm.db => P,R,S generate new PVs. G,So,Ro and others form the original PV names.
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=01,S=$(S1),G=$(G1),So=$(S1)-010,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=02,S=$(S1),G=$(G1),So=$(S1)-010,Ro=02,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=03,S=$(S1),G=$(G1),So=$(S1)-010,Ro=03,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
## skip number 4
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=05,S=$(S1),G=$(G1),So=$(S1)-010,Ro=05,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=06,S=$(S1),G=$(G1),So=$(S1)-010,Ro=06,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=07,S=$(S1),G=$(G1),So=$(S1)-010,Ro=07,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm2,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=08,S=$(S1),G=$(G1),So=$(S1)-010,Ro=08,AMC_NUM=140,FE_NUM=140,P_ATTN=Bpm1,PEVR=$(PEVR)")
# DTL-010 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=01,S=$(S2),G=$(G1),So=$(S2)-010,Ro=01,AMC_NUM=140,FE_NUM=140,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 140 Crate 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S1),G=$(G1),AMC_NUM=110,FE_NUM=110,BPM_PAIR=01-02")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S1),G=$(G1),AMC_NUM=120,FE_NUM=120,BPM_PAIR=03-05")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S1),G=$(G1),AMC_NUM=130,FE_NUM=130,BPM_PAIR=06-07")
# duplicate AMC-140 with both sections, shared AMC between MEBT and DTL
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S1),G=$(G1),AMC_NUM=140,FE_NUM=140,BPM_PAIR=08-01")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S2),G=$(G1),AMC_NUM=140,FE_NUM=140,BPM_PAIR=08-01")

# Section DB - S1: MEBT-010
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_section_$(S1).db","P=$(P),S=$(S1)")

############################################################################
# BPM01-IOC02 - DTL-010 5 BPMs + DTL-020 3 BPMs
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-201")  # EVR
epicsEnvSet("PEVR", "PBI-BPM01:Ctrl-$(EVR_DEV):")

# Single BPMs Crate 02 - DTL 02 03 04 05 06 07 08 09
# DTL-010 BPMs: 02 03 04 05 06
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=02,S=$(S2),G=$(G1),So=$(S2)-010,Ro=02,AMC_NUM=210,FE_NUM=150,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=03,S=$(S2),G=$(G1),So=$(S2)-010,Ro=03,AMC_NUM=210,FE_NUM=150,P_ATTN=Bpm2,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=04,S=$(S2),G=$(G1),So=$(S2)-010,Ro=04,AMC_NUM=220,FE_NUM=160,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=05,S=$(S2),G=$(G1),So=$(S2)-010,Ro=05,AMC_NUM=220,FE_NUM=160,P_ATTN=Bpm2,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=06,S=$(S2),G=$(G1),So=$(S2)-010,Ro=06,AMC_NUM=230,FE_NUM=170,P_ATTN=Bpm1,PEVR=$(PEVR)")
# DTL-020 BPMs: 01 02 03
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=07,S=$(S2),G=$(G1),So=$(S2)-020,Ro=01,AMC_NUM=230,FE_NUM=170,P_ATTN=Bpm2,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=08,S=$(S2),G=$(G1),So=$(S2)-020,Ro=02,AMC_NUM=240,FE_NUM=180,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=09,S=$(S2),G=$(G1),So=$(S2)-020,Ro=03,AMC_NUM=240,FE_NUM=180,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 210 to 240 Crate 02
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S2),G=$(G1),AMC_NUM=210,FE_NUM=150,BPM_PAIR=02-03")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S2),G=$(G1),AMC_NUM=220,FE_NUM=160,BPM_PAIR=04-05")
# TODO: check BPM FE PV for AMC-230 what is the section number (DTL-010 or DTL-020)
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S2),G=$(G1),AMC_NUM=230,FE_NUM=170,BPM_PAIR=06-07")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S2),G=$(G1),AMC_NUM=240,FE_NUM=180,BPM_PAIR=08-09")

############################################################################
# BPM02-IOC01 - DTL-030 2 BPMs + DTL-040 2 BPMs + DTL-050 2 BPMs
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G2", "BPM02")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G2):Ctrl-$(EVR_DEV):")

# Single BPMs Crate 03 - DTL 10 11 12 13 14 15
# DTL-030 BPMs: 01 02
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=10,S=$(S2),G=$(G2),So=$(S2)-030,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=11,S=$(S2),G=$(G2),So=$(S2)-030,Ro=02,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# DTL-040 BPMs: 01 02
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=12,S=$(S2),G=$(G2),So=$(S2)-040,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=13,S=$(S2),G=$(G2),So=$(S2)-040,Ro=02,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")
# DTL-050 BPMs: 01 02
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=14,S=$(S2),G=$(G2),So=$(S2)-050,Ro=01,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=15,S=$(S2),G=$(G2),So=$(S2)-050,Ro=02,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 130 Crate 03
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S2),G=$(G2),AMC_NUM=110,FE_NUM=110,BPM_PAIR=10-11")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S2),G=$(G2),AMC_NUM=120,FE_NUM=120,BPM_PAIR=12-13")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S2),G=$(G2),AMC_NUM=130,FE_NUM=130,BPM_PAIR=14-15")

# Section DB - S2: DTL
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_section_$(S2).db","P=$(P),S=$(S2)")

############################################################################
# BPM02-IOC02 - SPK-010 2 BPMs + SPK 020 030 040 050 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-201")  # EVR
epicsEnvSet("PEVR", "PBI-$(G2):Ctrl-$(EVR_DEV):")
epicsEnvSet("S3", "SPK")              # Section3: SPK

# Single BPMs Crate 04 - SPK 01 02 03 04 05 06
# SPK-010 BPMs: 01 02
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=01,S=$(S3),G=$(G2),So=$(S3)-010LWU,Ro=01,AMC_NUM=210,FE_NUM=140,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=02,S=$(S3),G=$(G2),So=$(S3)-010LWU,Ro=02,AMC_NUM=210,FE_NUM=140,P_ATTN=Bpm2,PEVR=$(PEVR)")
# SPK-020 BPMs: 01 / SPK-030 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=03,S=$(S3),G=$(G2),So=$(S3)-020LWU,Ro=01,AMC_NUM=220,FE_NUM=150,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=04,S=$(S3),G=$(G2),So=$(S3)-030LWU,Ro=01,AMC_NUM=220,FE_NUM=150,P_ATTN=Bpm2,PEVR=$(PEVR)")
# SPK-040 BPMs: 01 / SPK-050 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=05,S=$(S3),G=$(G2),So=$(S3)-040LWU,Ro=01,AMC_NUM=230,FE_NUM=160,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=06,S=$(S3),G=$(G2),So=$(S3)-050LWU,Ro=01,AMC_NUM=230,FE_NUM=160,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 210 to 230 Crate 04
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S3),G=$(G2),AMC_NUM=210,FE_NUM=140,BPM_PAIR=01-02")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S3),G=$(G2),AMC_NUM=220,FE_NUM=150,BPM_PAIR=03-04")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S3),G=$(G2),AMC_NUM=230,FE_NUM=160,BPM_PAIR=05-06")

############################################################################
# BPM03 - SPK 060 070 080 090 100 110 120 130 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G3", "BPM03")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G3):Ctrl-$(EVR_DEV):")

# Single BPMs Crate 05 - SPK 07 08 09 10 11 12 13 14
# SPK-060 BPMs: 01 / SPK-070 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=07,S=$(S3),G=$(G3),So=$(S3)-060LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=08,S=$(S3),G=$(G3),So=$(S3)-070LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# SPK-080 BPMs: 01 / SPK-090 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=09,S=$(S3),G=$(G3),So=$(S3)-080LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=10,S=$(S3),G=$(G3),So=$(S3)-090LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")
# SPK-100 BPMs: 01 / SPK-110 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=11,S=$(S3),G=$(G3),So=$(S3)-100LWU,Ro=01,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=12,S=$(S3),G=$(G3),So=$(S3)-110LWU,Ro=01,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm2,PEVR=$(PEVR)")
# SPK-120 BPMs: 01 / SPK-130 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=13,S=$(S3),G=$(G3),So=$(S3)-120LWU,Ro=01,AMC_NUM=140,FE_NUM=140,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=14,S=$(S3),G=$(G3),So=$(S3)-130LWU,Ro=01,AMC_NUM=140,FE_NUM=140,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 140 Crate 05
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S3),G=$(G3),AMC_NUM=110,FE_NUM=110,BPM_PAIR=07-08")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S3),G=$(G3),AMC_NUM=120,FE_NUM=120,BPM_PAIR=09-10")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S3),G=$(G3),AMC_NUM=130,FE_NUM=130,BPM_PAIR=11-12")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S3),G=$(G3),AMC_NUM=140,FE_NUM=140,BPM_PAIR=13-14")

# Section DB - S3: SPK
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_section_$(S3).db","P=$(P),S=$(S3)")

############################################################################
# BPM04 - MBL 010 020 030 040 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G4", "BPM04")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G4):Ctrl-$(EVR_DEV):")
epicsEnvSet("S4", "MBL")              # Section4: MBL

# Single BPMs Crate 06 - MBL 01 02 03 04
# MBL-010 BPMs: 01 / MBL-020 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=01,S=$(S4),G=$(G4),So=$(S4)-010LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=02,S=$(S4),G=$(G4),So=$(S4)-020LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# MBL-030 BPMs: 01 / MBL-040 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=03,S=$(S4),G=$(G4),So=$(S4)-030LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=04,S=$(S4),G=$(G4),So=$(S4)-040LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 120 Crate 06
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S4),G=$(G4),AMC_NUM=110,FE_NUM=110,BPM_PAIR=01-02")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S4),G=$(G4),AMC_NUM=120,FE_NUM=120,BPM_PAIR=03-04")

############################################################################
# BPM05 - MBL 050 060 070 080 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G5", "BPM05")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G5):Ctrl-$(EVR_DEV):")

# Single BPMs Crate 07 - MBL 05 06 07 08
# MBL-050 BPMs: 01 / MBL-060 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=05,S=$(S4),G=$(G5),So=$(S4)-050LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=06,S=$(S4),G=$(G5),So=$(S4)-060LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# MBL-070 BPMs: 01 / MBL-080 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=07,S=$(S4),G=$(G5),So=$(S4)-070LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=08,S=$(S4),G=$(G5),So=$(S4)-080LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 120 Crate 07
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S4),G=$(G5),AMC_NUM=110,FE_NUM=110,BPM_PAIR=05-06")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S4),G=$(G5),AMC_NUM=120,FE_NUM=120,BPM_PAIR=07-08")

############################################################################
# BPM06 - MBL 090 1 BPM + HBL 010 020 030 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G6", "BPM06")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G5):Ctrl-$(EVR_DEV):")
epicsEnvSet("S5", "HBL")              # Section5: HBL

# Single BPMs Crate 08 - MBL 09 + HBL 01 02 03
# MBL-090 BPMs: 01 / HBL-010 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=09,S=$(S4),G=$(G6),So=$(S4)-090LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=01,S=$(S5),G=$(G6),So=$(S5)-010LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# HBL-020 BPMs: 01 / HBL-030 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=02,S=$(S5),G=$(G6),So=$(S5)-020LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=03,S=$(S5),G=$(G6),So=$(S5)-030LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 120 Crate 08
# duplicate AMC-110 with both sections, shared AMC between MBL and HBL
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S4),G=$(G6),AMC_NUM=110,FE_NUM=110,BPM_PAIR=09-01")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S5),G=$(G6),AMC_NUM=110,FE_NUM=110,BPM_PAIR=09-01")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S5),G=$(G6),AMC_NUM=120,FE_NUM=120,BPM_PAIR=02-03")

# Section DB - S4: MBL
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_section_$(S4).db","P=$(P),S=$(S4)")

############################################################################
# BPM07 - HBL 040 050 060 070 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G7", "BPM07")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G7):Ctrl-$(EVR_DEV):")

# Single BPMs Crate 09 - HBL 04 05 06 07
# HBL-040 BPMs: 01 / HBL-050 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=04,S=$(S5),G=$(G7),So=$(S5)-040LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=05,S=$(S5),G=$(G7),So=$(S5)-050LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# HBL-060 BPMs: 01 / HBL-070 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=06,S=$(S5),G=$(G7),So=$(S5)-060LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=07,S=$(S5),G=$(G7),So=$(S5)-070LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 120 Crate 09
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S5),G=$(G7),AMC_NUM=110,FE_NUM=110,BPM_PAIR=04-05")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S5),G=$(G7),AMC_NUM=120,FE_NUM=120,BPM_PAIR=06-07")

############################################################################
# BPM08 - HBL 080 090 100 110 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G8", "BPM08")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G8):Ctrl-$(EVR_DEV):")

# Single BPMs Crate 10 - HBL 08 09 10 11
# HBL-080 BPMs: 01 / HBL-090 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=08,S=$(S5),G=$(G8),So=$(S5)-080LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=09,S=$(S5),G=$(G8),So=$(S5)-090LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# HBL-100 BPMs: 01 / HBL-110 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=10,S=$(S5),G=$(G8),So=$(S5)-100LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=11,S=$(S5),G=$(G8),So=$(S5)-110LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 120 Crate 10
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S5),G=$(G8),AMC_NUM=110,FE_NUM=110,BPM_PAIR=08-09")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S5),G=$(G8),AMC_NUM=120,FE_NUM=120,BPM_PAIR=10-11")

############################################################################
# BPM09 - HBL 120 130 140 150 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G9", "BPM09")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G9):Ctrl-$(EVR_DEV):")

# Single BPMs Crate 11 - HBL 12 13 14 15
# HBL-120 BPMs: 01 / HBL-130 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=12,S=$(S5),G=$(G9),So=$(S5)-120LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=13,S=$(S5),G=$(G9),So=$(S5)-130LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# HBL-140 BPMs: 01 / HBL-150 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=14,S=$(S5),G=$(G9),So=$(S5)-140LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=15,S=$(S5),G=$(G9),So=$(S5)-150LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 120 Crate 11
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S5),G=$(G9),AMC_NUM=110,FE_NUM=110,BPM_PAIR=12-13")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S5),G=$(G9),AMC_NUM=120,FE_NUM=120,BPM_PAIR=14-15")

############################################################################
# BPM10 - HBL 160 170 180 190 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G10", "BPM10")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G10):Ctrl-$(EVR_DEV):")

# Single BPMs Crate 12 - HBL 16 17 18 19
# HBL-160 BPMs: 01 / HBL-170 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=16,S=$(S5),G=$(G10),So=$(S5)-160LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=17,S=$(S5),G=$(G10),So=$(S5)-170LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# HBL-180 BPMs: 01 / HBL-190 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=18,S=$(S5),G=$(G10),So=$(S5)-180LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=19,S=$(S5),G=$(G10),So=$(S5)-190LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 120 Crate 12
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S5),G=$(G10),AMC_NUM=110,FE_NUM=110,BPM_PAIR=16-17")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S5),G=$(G10),AMC_NUM=120,FE_NUM=120,BPM_PAIR=18-19")

############################################################################
# BPM11 - HBL 200 210 + HEBT 010 020 030 040 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G11", "BPM11")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G11):Ctrl-$(EVR_DEV):")
epicsEnvSet("S6", "HEBT")              # Section6: HEBT

# Single BPMs Crate 13 - HBL 20 21 + HEBT 01 02 03 04
# HBL-200 BPMs: 01 / HBL-210 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=20,S=$(S5),G=$(G11),So=$(S5)-200LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=21,S=$(S5),G=$(G11),So=$(S5)-210LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# HEBT-010 BPMs: 01 / HEBT-020 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=01,S=$(S6),G=$(G11),So=$(S6)-010LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=02,S=$(S6),G=$(G11),So=$(S6)-020LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")
# HEBT-030 BPMs: 01 / HEBT-040 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=03,S=$(S6),G=$(G11),So=$(S6)-030LWU,Ro=01,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=04,S=$(S6),G=$(G11),So=$(S6)-040LWU,Ro=01,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 130 Crate 13
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S5),G=$(G11),AMC_NUM=110,FE_NUM=110,BPM_PAIR=20-21")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S6),G=$(G11),AMC_NUM=120,FE_NUM=120,BPM_PAIR=01-02")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S6),G=$(G11),AMC_NUM=130,FE_NUM=130,BPM_PAIR=03-04")

# Section DB - S5: HBL
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_section_$(S5).db","P=$(P),S=$(S5)")

############################################################################
# BPM12 - HEBT 050 060 070 080 090 100 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G12", "BPM12")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G12):Ctrl-$(EVR_DEV):")

# Single BPMs Crate 14 - HEBT 05 06 07 08 09 10
# HEBT-050 BPMs: 01 / HEBT-060 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=05,S=$(S6),G=$(G12),So=$(S6)-050LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=06,S=$(S6),G=$(G12),So=$(S6)-060LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# HEBT-070 BPMs: 01 / HEBT-080 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=07,S=$(S6),G=$(G12),So=$(S6)-070LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=08,S=$(S6),G=$(G12),So=$(S6)-080LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")
# HEBT-090 BPMs: 01 / HEBT-100 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=09,S=$(S6),G=$(G12),So=$(S6)-090LWU,Ro=01,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=10,S=$(S6),G=$(G12),So=$(S6)-100LWU,Ro=01,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 130 Crate 14
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S6),G=$(G12),AMC_NUM=110,FE_NUM=110,BPM_PAIR=05-06")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S6),G=$(G12),AMC_NUM=120,FE_NUM=120,BPM_PAIR=07-08")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S6),G=$(G12),AMC_NUM=130,FE_NUM=130,BPM_PAIR=09-10")

############################################################################
# BPM13-IOC01 - HEBT 110 120 130 140 150 160 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G13", "BPM13")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G13):Ctrl-$(EVR_DEV):")

# Single BPMs Crate 15 - HEBT 11 12 13 14 15 16 
# HEBT-110 BPMs: 01 / HEBT-120 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=11,S=$(S6),G=$(G13),So=$(S6)-110LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=12,S=$(S6),G=$(G13),So=$(S6)-120LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# HEBT-130 BPMs: 01 / HEBT-140 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=13,S=$(S6),G=$(G13),So=$(S6)-130LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=14,S=$(S6),G=$(G13),So=$(S6)-140LWU,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")
# HEBT-150 BPMs: 01 / HEBT-160 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=15,S=$(S6),G=$(G13),So=$(S6)-150LWU,Ro=01,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=16,S=$(S6),G=$(G13),So=$(S6)-160LWU,Ro=01,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 130 Crate 15
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S6),G=$(G13),AMC_NUM=110,FE_NUM=110,BPM_PAIR=11-12")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S6),G=$(G13),AMC_NUM=120,FE_NUM=120,BPM_PAIR=13-14")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S6),G=$(G13),AMC_NUM=130,FE_NUM=130,BPM_PAIR=15-16")

# Section DB - S6: HEBT
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_section_$(S6).db","P=$(P),S=$(S6)")

############################################################################
# BPM13-IOC02 - A2T 010 020 + DmpL 010 020 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-201")  # EVR
epicsEnvSet("PEVR", "PBI-$(G13):Ctrl-$(EVR_DEV):")
epicsEnvSet("S7", "A2T")              # Section7: A2T 
epicsEnvSet("S8", "DmpL")             # Section8: DmpL

# Single BPMs Crate 16 - A2T 01 02 + DmpL 01 02
# A2T-010 BPMs: 01 / A2T-020 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=01,S=$(S7),G=$(G13),So=$(S7)-010LWU,Ro=01,AMC_NUM=210,FE_NUM=140,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=02,S=$(S7),G=$(G13),So=$(S7)-020LWU,Ro=01,AMC_NUM=210,FE_NUM=140,P_ATTN=Bpm2,PEVR=$(PEVR)")
# DmpL-010 BPMs: 01 / DmpL-020 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=01,S=$(S8),G=$(G13),So=$(S8)-010QC, Ro=01,AMC_NUM=220,FE_NUM=150,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=02,S=$(S8),G=$(G13),So=$(S8)-020QC, Ro=01,AMC_NUM=220,FE_NUM=150,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 120 Crate 16
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S7),G=$(G13),AMC_NUM=210,FE_NUM=140,BPM_PAIR=01-02")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S8),G=$(G13),AMC_NUM=220,FE_NUM=150,BPM_PAIR=01-02")

############################################################################
# BPM14-IOC01 - A2T 030 040 + DmpL 030 040 + A2T 050 060 1 BPM
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-101")  # EVR
epicsEnvSet("G14", "BPM14")            # BPM Group
epicsEnvSet("PEVR", "PBI-$(G14):Ctrl-$(EVR_DEV):")

# Single BPMs Crate 17 - A2T 03 04 + DmpL 03 04 + A2T 05 06 
# A2T-030 BPMs: 01 / A2T-040 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=03,S=$(S7),G=$(G14),So=$(S7)-030LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=04,S=$(S7),G=$(G14),So=$(S7)-040LWU,Ro=01,AMC_NUM=110,FE_NUM=110,P_ATTN=Bpm2,PEVR=$(PEVR)")
# DmpL-030 BPMs: 01 / DmpL-040 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=03,S=$(S8),G=$(G14),So=$(S8)-030QC, Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=04,S=$(S8),G=$(G14),So=$(S8)-040Drf,Ro=01,AMC_NUM=120,FE_NUM=120,P_ATTN=Bpm2,PEVR=$(PEVR)")
# A2T-050 BPMs: 01 / A2T-060 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=05,S=$(S7),G=$(G14),So=$(S7)-050LWU,Ro=01,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=06,S=$(S7),G=$(G14),So=$(S7)-060LWU,Ro=01,AMC_NUM=130,FE_NUM=130,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 130 Crate 17
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S7),G=$(G14),AMC_NUM=110,FE_NUM=110,BPM_PAIR=03-04")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S8),G=$(G14),AMC_NUM=120,FE_NUM=120,BPM_PAIR=03-04")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S7),G=$(G14),AMC_NUM=130,FE_NUM=130,BPM_PAIR=05-06")

############################################################################
# BPM14-IOC02 - A2T 080 090 + A2T 120Drf 130LWU + A2T 130LWU 2 130NSW
############################################################################
epicsEnvSet("EVR_DEV",    "EVR-201")  # EVR
epicsEnvSet("PEVR", "PBI-$(G14):Ctrl-$(EVR_DEV):")

# Single BPMs Crate 18 - A2T 07 08 09 10 11 12
# A2T-080 BPMs: 01 / A2T-090 BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=07,S=$(S7),G=$(G14),So=$(S7)-080LWU,Ro=01,AMC_NUM=210,FE_NUM=140,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=08,S=$(S7),G=$(G14),So=$(S7)-090LWU,Ro=01,AMC_NUM=210,FE_NUM=140,P_ATTN=Bpm2,PEVR=$(PEVR)")
# A2T-120Drf BPMs: 01 / A2T-130LWU BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=09,S=$(S7),G=$(G14),So=$(S7)-120Drf,Ro=01,AMC_NUM=220,FE_NUM=150,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=10,S=$(S7),G=$(G14),So=$(S7)-130LWU,Ro=01,AMC_NUM=220,FE_NUM=150,P_ATTN=Bpm2,PEVR=$(PEVR)")
# A2T-130LWU BPMs: 02 / A2T-130NSW BPMs: 01
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=11,S=$(S7),G=$(G14),So=$(S7)-130LWU,Ro=02,AMC_NUM=230,FE_NUM=160,P_ATTN=Bpm1,PEVR=$(PEVR)")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_bpm.db","P=$(P),R=12,S=$(S7),G=$(G14),So=$(S7)-130NSW,Ro=01,AMC_NUM=230,FE_NUM=160,P_ATTN=Bpm2,PEVR=$(PEVR)")

# Single AMCs 110 to 130 Crate 18
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S7),G=$(G14),AMC_NUM=210,FE_NUM=140,BPM_PAIR=07-08")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S7),G=$(G14),AMC_NUM=220,FE_NUM=150,BPM_PAIR=09-10")
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_single_amc.db","P=$(P),S=$(S7),G=$(G14),AMC_NUM=230,FE_NUM=160,BPM_PAIR=11-12")

# Section DB - S7: A2T
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_section_$(S7).db","P=$(P),S=$(S7)")

# Section DB - S8: DmpL
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_section_$(S8).db","P=$(P),S=$(S8)")

############################################################################

# All Sections DB
dbLoadRecords("$(E3_CMD_TOP)/db/bpmmanager_section_ALL.db","P=$(P),S=ALL")


############################################################################
iocInit()

